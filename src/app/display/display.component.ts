import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {

  private httpOptions: any;

  welcomeAPI = `${environment.apiUrl}`;
  usersAPI = `${environment.apiUrl}users`;
  postAPI =  `${environment.apiUrl}newuser`;

  profileForm = this.fb.group({
    id: ['', Validators.required],
    username: ['', Validators.required],
    name: ['', Validators.required],
    position: ['', Validators.required]
  });

  data: any[];

  constructor(
    private fb: FormBuilder,
    private route: Router,
    private http: HttpClient
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      observe: 'events'
    };
  }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    return this.http.get(this.usersAPI, {responseType: 'text'}).subscribe(res => {
      console.log('GET Resquest is successful!', res);
    }, error => {
      console.log(error);
    });
  }

  getUserData() {
    return this.http.get(this.welcomeAPI, {responseType: 'text'}).subscribe(res => {
      console.log('GET Request is successful!', res);
    }, error => {
      console.warn(error);
    });
  }

  addUserData() {
    const JSONProfileForm = JSON.stringify(this.profileForm.value);
    return this.http.post<string[]>(this.postAPI, JSONProfileForm).subscribe(res => {
      this.data = res;
      console.warn('POST Request is successful', res);
      // console.warn(this.profileForm.value);
    }, error => {
      console.log(error);
    });
  }
}
